# Thesis Handin


Install this project.

```
git submodule init
```

```
git submodule update --remote --recursive --force
```

Go to Gazebo and run the same repo updates:

```
git submodule init
```

```
git submodule update --remote --recursive --force
```

Build the PX4 library:

```
./build_PX4
```

Go to catkin_ws and build

```
cd catkin_ws
catkin build
```

source the catkin library

```
source catkin_ws/devel/setup.bash
```

run the program.
```
./Gazebo/startup_new.sh
```
in a new terminal start the mavros protocol
```
roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"
```

lastly, run the program to precisionland:

```
rosrun control Detection.py
```

in a new terminal run the controller
```
rosrun control Controller.py
```

