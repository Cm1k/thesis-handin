#!/usr/bin/env python2
from __future__ import print_function
from cv2 import aruco
import os
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import yaml
import numpy as np
import matplotlib.pyplot as plt
import roslib
# roslib.load_manifest('my_package')
import rospkg
rospack = rospkg.RosPack()

ARUCO_PARAMETERS = aruco.DetectorParameters_create()
aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
board = aruco.GridBoard_create(
        markersX=4,
        markersY=6,
        markerLength=0.04,
        markerSeparation=0.01,
        dictionary=aruco_dict)


class image_converter:
    def __init__(self):
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/webcam/image_raw", Image, self.callback)
        self.mtx = 0
        self.dist = 0
        self.getDistortionMatrix()

    def getDistortionMatrix(self):
        stream = open(os.path.join(rospack.get_path("control"), "src", "vision", "calibration_matrix.yaml"), 'r')
        dict = yaml.load(stream)
        for key, value in dict.items():
            if key == "dist_coeff":
              print (str(value))
              self.dist = np.asarray(value)
            if key == "camera_matrix":
              print (str(value))
              self.mtx = np.asarray(value)

    def undistort_img(self, img):
        h, w = img.shape[:2]
        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(self.mtx, self.dist, (w, h), 1, (w, h))
        # undistort
        dst = cv2.undistort(img, self.mtx, self.dist, None, newcameramtx)

        # crop the image
        x, y, w, h = roi
        dst = dst[y:y + h, x:x + w]
        # cv2.imshow("Calibration", dst)
        return dst

    def ArUco_detection(self, frame):

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # set dictionary size depending on the aruco marker selected
        aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)

        # detector parameters can be set here (List of detection parameters[3])
        parameters = aruco.DetectorParameters_create()
        parameters.adaptiveThreshConstant = 2


        # lists of ids and the corners belonging to each id
        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

        # font for displaying text (below)
        font = cv2.FONT_HERSHEY_SIMPLEX

        # check if the ids list is not empty
        # if no check is added the code will crash
        if np.all(ids != None):

            # estimate pose of each marker and return the values
            # rvet and tvec-different from camera coefficients
            rvec, tvec, _ = aruco.estimatePoseSingleMarkers(corners, 0.05, self.mtx, self.dist)
            # (rvec-tvec).any() # get rid of that nasty numpy value array error

            for i in range(0, ids.size):
                # draw axis for the aruco markers
                aruco.drawAxis(frame, self.mtx, self.dist, rvec[i], tvec[i], 0.05)

            # draw a square around the markers
            aruco.drawDetectedMarkers(frame, corners)
            # aruco.refineDetectedMarkers(detectedCorners=corners, detectedIds=ids, image=frame, rejectedCorners=rejectedImgPoints, )

        return frame

    def ArUco_board_detection(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Detect Aruco markers
        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=ARUCO_PARAMETERS)

        # Refine detected markers
        # Eliminates markers not part of our board, adds missing markers to the board
        corners, ids, rejectedImgPoints, recoveredIds = aruco.refineDetectedMarkers(
            image=gray,
            board=board,
            detectedCorners=corners,
            detectedIds=ids,
            rejectedCorners=rejectedImgPoints,
            cameraMatrix=self.mtx,
            distCoeffs=self.dist)

        ###########################################################################
        # TODO: Add validation here to reject IDs/corners not part of a gridboard #
        ###########################################################################

        # Outline all of the markers detected in our image
        QueryImg = aruco.drawDetectedMarkers(frame, corners, borderColor=(0, 0, 255))

        # Require 15 markers before drawing axis
        if ids is not None and len(ids) > 2:
            # Estimate the posture of the gridboard, which is a construction of 3D space based on the 2D video
            pose, rvec, tvec = aruco.estimatePoseBoard(corners, ids, board, self.mtx, self.dist)
            if pose:
                # Draw the camera posture calculated from the gridboard
                QueryImg = aruco.drawAxis(QueryImg, self.mtx, self.dist, rvec, tvec, 0.05)
                print(tvec)
        # Display our image
        return QueryImg

    def callback(self,data):
        # print("test")
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            cal_img = self.undistort_img(cv_image)
            #cv2.imshow("ArUco detection", self.ArUco_detection(cal_img))
            cv2.imshow("ArUco detection", self.ArUco_board_detection(cal_img))
        except CvBridgeError as e:
            print(e)
        cv2.waitKey(1)


def main(args):
    ic = image_converter()
    rospy.init_node('image_converter', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
        # cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
