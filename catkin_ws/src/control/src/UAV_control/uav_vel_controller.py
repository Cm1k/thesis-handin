#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$

#!/usr/bin/python
from simple_pid import PID


class velocity_control:
    def __init__(self):
        self.dist_pid       = PID()
        self.lat_pid        = PID()
        self.altitude_pid    = PID()
        self.dist_tunes = 0
        self.lat_tunes = 0
        self.altitude_tunes = 0

    def dist_pidSetup(self, p=1, i=0, d=0, sampletime=0.1, outputLimits=(-0.5, 0.5), automode=False, setpoint=0):
        self.dist_pid.tunings = (p, i, d)
        self.dist_tunes = p, i, d
        self.dist_pid.sample_time = sampletime
        self.dist_pid.output_limits = outputLimits
        self.dist_pid.auto_mode = automode
        self.dist_pid.setpoint = setpoint

    def update_distPID(self, update_variable):
        return self.dist_pid(update_variable)

    def lat_pidSetup(self, p=1, i=0, d=0, sampletime=0.1, outputLimits=(-0.5, 0.5), automode=False, setpoint=0):
        self.lat_pid.tunings = (p, i, d)
        self.lat_tunes = p, i, d
        self.lat_pid.sample_time = sampletime
        self.lat_pid.output_limits = outputLimits
        self.lat_pid.auto_mode = automode
        self.lat_pid.setpoint = setpoint

    def update_latPID(self, update_variable):
        return self.lat_pid(update_variable)

    def altitude_pidSetup(self, p=1, i=0, d=0, sampletime=0.1, outputLimits=(-0.5, 0.5), automode=False, setpoint=0):
        self.altitude_pid.tunings = (p, i, d)
        self.altitude_tunes = p, i, d
        self.altitude_pid.sample_time = sampletime
        self.altitude_pid.output_limits = outputLimits
        self.altitude_pid.auto_mode = automode
        self.altitude_pid.setpoint = setpoint

    def update_altitudePID(self, update_variable):
        return self.altitude_pid(update_variable)

    def get_dist_tunes(self):
        return self.dist_tunes

    def get_lat_tunes(self):
        return self.lat_tunes

    def get_altitude_tunes(self):
        return self.altitude_tunes
