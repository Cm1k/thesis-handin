#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$

# libraries

'''
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv


data = pd.read_csv("test.csv", usecols=[0, 1, 2])
data.x.plot(color='g', lw=1.3)
# data.y.plot(color='r', lw=1.3)
# data.z.plot(color='b', lw=1.3)

plt.xlabel("Measuring points")
plt.ylabel("Error in meters")

plt.plot([0, 10000], [-0.05, -0.05], label='Lower boundary')
plt.plot([0, 10000], [0.05, 0.05], label='Upper boundary')
plt.legend()
plt.show(block=True)
'''


import pylab as plt
import numpy as np
import queue

import matplotlib.pyplot as plot
import matplotlib.animation as animation
from matplotlib import style

# style.use('fivethirtyeight')


class plotting:
    def __init__(self):
        self.datapoints = np.linspace(0, 500, 500)
        self.x = np.zeros(shape=self.datapoints.shape)
        self.y = []
        self.z = []
        self.counter = 0
        plt.ion()
        self.q1 = queue.LifoQueue(500)
        while not self.q1.full():
            self.q1.put(0)

    def setNewValues(self, x_new, y_new, z_new):
        self.x = np.append(self.x, [x_new], axis=0)
        self.x = np.delete(self.x, 0, axis=0)
        self.y.append(y_new)
        self.z.append(z_new)

    def graphLive(self):
        plt.ylim(-0, 2)
        graph = plt.plot(self.datapoints, self.x)[0]
        plt.plot([0, 500], [-0.05, -0.05], label='Lower boundary', linestyle='--')
        plt.plot([0, 500], [0.05, 0.05], label='Upper boundary', linestyle='--')
        plt.legend()
        plt.show()
        while True:
            graph.set_ydata(self.x)
            plt.draw()
            plt.pause(0.1)

#
# class matPlotting:
#     def __init__(self):
#         self.fig = plot.figure()
#         self.ax1 = self.fig.add_subplot(1, 1, 1)
#         self.x = []
#         self.y = []
#
#     def animate(self, i):
#         print("animate")
#         self.ax1.clear()
#         self.ax1.plot(self.x, self.y)
#
#     def new_vals(self, x, y):
#         print("new_vals")
#         self.x.append(float(x))
#         self.y.append(float(y))
#         self.ax1.clear()
#         self.ax1.plot(x, y)
#
#     def start_ani(self):
#         print("Start_ani")
#         ani = animation.FuncAnimation(self.fig, self.animate, interval=1000)
#         plot.show()
