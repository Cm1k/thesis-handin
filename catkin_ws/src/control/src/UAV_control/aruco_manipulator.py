#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$

import rospy
import cv2
import tf
from control.msg import pos, aruco_center
import math
import numpy as np
import std_msgs.msg
import csv


class aruco_det:
    def __init__(self):
        self.aruco_center = rospy.Publisher("aruco_center", aruco_center, queue_size=10)
        self.ArUco_position = rospy.Publisher('ArUco_pos', pos, queue_size=10)
        self.board_center = aruco_center()
        self.current_center = [0, 0]
        self.broadcaster = tf.TransformBroadcaster()
        self.Aruco_pos = pos()

    def drawArucoCenter(self, image, corners):

        self.board_center.x = 0
        self.board_center.y = 0

        board_center_x = 0
        board_center_y = 0

        # Add all corners from the detected marker to a variable.
        for i in range(0, len(corners)):
            try:
                board_center_x += (corners[i][0][3][0] + corners[i][0][1][0]) / 2
                board_center_y += (corners[i][0][3][1] + corners[i][0][1][1]) / 2
            except:
                pass

        # Get the center of the marker board.
        if board_center_x != 0:
            self.current_center[0] = int(board_center_x / len(corners))
            self.board_center.x = int(board_center_x / len(corners))
        if board_center_y != 0:
            self.current_center[1] = int(board_center_y / len(corners))
            self.board_center.y = int(board_center_y / len(corners))
        # Draw a circle on the drone camera at the marker center. publish it to ros.
        if board_center_y != 0 and board_center_x != 0:
            cv2.circle(image, (self.current_center[0], self.current_center[1]), radius=5, thickness=10,
                       color=(0, 255, 0))
            self.aruco_center.publish(self.board_center)

    def rotateVec(self, axis, deg, vec):

        wished = [[-1.0000000, 0.0000000, 0.0000000],
                  [0.0000000, 1.0000000, 0.0000000],
                  [-0.0000000, 0.0000000, -1.0000000]]
        new_vec = np.dot(vec, wished)

        return cv2.Rodrigues(new_vec)[0]

    @staticmethod
    def log_info(x, y, z, filename):
        """Logging information."""
        with open(filename, mode='a') as uav_attitude_file:
            attitude_writer = csv.writer(uav_attitude_file, delimiter=',')
            attitude_writer.writerow([x, y, z])

    def ArUco_board_detection(self, frame, k, d, aruco_dict, aruco_parameters, board):
        """
        :param frame:           the image to detect
        :param k:               the camera matrix of the camera
        :param d:               the distance coeficcients gotten from the camera calibration.
        :param aruco_dict:      the dictionary in which the aruco board(s) recides.
        :param aruco_parameters:the aruco parameters.
        :param board:           the board to detect."""
        board_detected = std_msgs.msg.Bool()
        board_detected.data = False
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Detect Aruco markers
        corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray, aruco_dict, parameters=aruco_parameters)

        # Refine detected markers
        # Eliminates markers not part of our board, adds missing markers to the board
        corners, ids, rejectedImgPoints, recoveredIds = cv2.aruco.refineDetectedMarkers(
            image=gray,
            board=board,
            detectedCorners=corners,
            detectedIds=ids,
            rejectedCorners=rejectedImgPoints,
            cameraMatrix=k,
            distCoeffs=d)

        # Outline all of the markers detected in our image
        # Require 12 markers before drawing axis

        self.drawArucoCenter(image=frame, corners=corners)

        QueryImg = cv2.aruco.drawDetectedMarkers(frame, corners, borderColor=(0, 0, 255))
        if ids is not None:
            # Estimate the posture of the gridboard, which is a construction of 3D space based on the 2D video
            pose, rvec, tvec = cv2.aruco.estimatePoseBoard(corners, ids, board, k, d)
            if pose:
                # print(tvec[0], tvec[1], tvec[2])

                board_detected.data = True
                # Draw the camera posture calculated from the gridboard
                QueryImg = cv2.aruco.drawAxis(QueryImg, k, d, rvec, tvec, 1)
                # print(tvec)
                dst, jac = cv2.Rodrigues(rvec)
                new_tvec = np.array([0, 0, -5])
                new_tvec = np.dot(dst, new_tvec)

                roll = math.atan2(dst[1][0], dst[0][0])
                yaw = math.atan2(-dst[2][0], math.sqrt(math.pow(dst[2][1], 2) + math.pow(dst[2][2], 2)))
                pitch = math.atan2(dst[2][1], dst[2][2])

                new_rvec = self.rotateVec(0, 180, dst)

                test = np.dot(dst, tvec)

                self.broadcaster.sendTransform(test,
                                               tf.transformations.quaternion_from_euler(rvec[0], rvec[1], rvec[2]),
                                               rospy.Time.now(), "aruco", "world")

                self.broadcaster.sendTransform(new_tvec,
                                               tf.transformations.quaternion_from_euler(new_rvec[0], new_rvec[1],
                                                                                        new_rvec[2]), rospy.Time.now(),
                                               "pos", "aruco")

                # cv2.putText(QueryImg, "roll: {}".format((roll)), (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 1,
                #             (255, 255, 255), lineType=2)
                # cv2.putText(QueryImg, "pitch: {}".format((pitch)), (0, 60), cv2.FONT_HERSHEY_SIMPLEX, 1,
                #             (255, 255, 255), lineType=2)
                # cv2.putText(QueryImg, "yaw: {}".format((yaw)), (0, 90), cv2.FONT_HERSHEY_SIMPLEX, 1,
                #             (255, 255, 255), lineType=2)
                # cv2.putText(QueryImg, "tvec: {}".format(test[0][0]), (0, 120), cv2.FONT_HERSHEY_SIMPLEX, 1,
                #             (255, 255, 255), lineType=2)
                # Send the aruco to camera transformation.

                # Send the aruco pos, might not be needed in the future.
                self.Aruco_pos.x = tvec[0]
                self.Aruco_pos.y = tvec[1]
                self.Aruco_pos.z = tvec[2]
                self.Aruco_pos.roll = math.degrees(roll)
                self.Aruco_pos.pitch = math.degrees(pitch)
                self.Aruco_pos.yaw = math.degrees(yaw)
                self.ArUco_position.publish(self.Aruco_pos)
                return QueryImg, board_detected


        return frame, board_detected
