#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$

#!/usr/bin/python
from control.msg import aruco_center
import rospy
from threading import Thread
from offboard_control import uav_offb


class UAV_stateMachine:

    def __init__(self):
        rospy.Subscriber("aruco_center", aruco_center, self.aruco_center_callback)

        self.uav_offboard = uav_offb()
        self.pos_thread = Thread(target=self.uav_offboard.send_pos, args=())
        self.pos_thread.daemon = True
        self.vel_con = Thread(target=self.uav_offboard.vel_control, args=())
        self.vel_con.daemon = True
        self.hover = Thread(target=self.uav_offboard.hover, args=())
        self.hover.daemon = True

        self.arucoBoard_center = [0, 0, 0, 0]

    def aruco_center_callback(self, data):
        """Callback from Detection script, when the marker is found,
        these data are used for pointing the UAV directly towards the marker center."""
        self.arucoBoard_center[0] = data.x
        self.arucoBoard_center[1] = data.y
        self.arucoBoard_center[2] = data.img_width
        self.arucoBoard_center[3] = data.img_height

    def switch(self, state):
        default = 0
        return getattr(self, 'case_' + str(state), lambda: default)()

    def case_0(self):
        rospy.loginfo("State 0")
        self.pos_thread.start()
        return 0

    def case_1(self):
        rospy.loginfo("State 1")
        self.uav_offboard.set_offboard_pos(0, 10, 2)
        return 1

    def case_2(self):
        self.uav_offboard.stop_pos = True
        self.vel_con.start()
        rospy.loginfo("State 2")
        return 2

    def case_3(self):
        rospy.loginfo("State 3")
        self.hover.start()
        return 3

    def case_4(self):
        rospy.loginfo("State 4")
        self.uav_offboard.PIDs.altitude_pid.auto_mode = True
        return 4

    def case_5(self):
        rospy.loginfo("State 5")
        self.uav_offboard.PIDs.lat_pid.auto_mode = True
        return 5

    def case_6(self):
        rospy.loginfo("State 6")
        self.uav_offboard.PIDs.dist_pid.auto_mode = True
        return 6

    ''' def case_7(self):
        rospy.loginfo("State 7")
        self.uav_offboard.PIDs.altitude_pid.setpoint = 3
        return 7'''

    ''' def case_8(self):
            rospy.loginfo("State 7")
            self.uav_offboard.PIDs.altitude_pid.setpoint = 5
            return 7'''

    def case_7(self):
        rospy.loginfo("State 7")
        self.uav_offboard.PIDs.altitude_pid.setpoint = 5
        return 7
